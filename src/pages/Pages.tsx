import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { LandingPage } from './Landing/Landing';
import { CpmoPage } from './Cpmo/Cpmo';
import { Header } from './Landing/components/Header/Header';

export class Page extends React.Component {
	render() {
		return (
			<Router>
				<Header />
				<Route
					path='/'
					exact
					render={() => {
						return <Redirect to='/landing-page' />;
					}}
				/>
				<Route
					path='/dashboard'
					exact
					strict // it is used strictly for http://localhost:8000/landing but not http://localhost:8000/landing/ will not work in strict mode
					component={CpmoPage}
				/>
				<Route path='/landing-page' exact component={LandingPage} />
				<Route path='/cpmo' exact component={CpmoPage} />
				<Route path='/settings' exact component={CpmoPage} />
			</Router>
		);
	}
}
