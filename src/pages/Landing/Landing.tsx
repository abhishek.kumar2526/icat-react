import './Landing.scss';
import axios from 'axios';
import React from 'react';
import { BASE } from './../../config/baseUrl';
import { ContractTable } from './components/ContractTable/ContractTable';
import { FilterContract } from './components/FilterContract/FilterContract';
import { ConfirmDialog } from './../../shared/components/ConfirmDialog/ConfirmDialog';
import { Loader } from '../../shared/components/Loader/Loader';

interface MyComponentProps {
	/* declare your component's props here */
}
interface MyComponentState {
	contractData: Array<any>;
	contarctRequest: Object;
	showLoader: Boolean;
	searchIcon: Boolean;
	query: String;
	sortType: String;
	showDialog: Boolean;
}

export class LandingPage extends React.Component<
	MyComponentProps,
	MyComponentState
> {
	constructor(props: any) {
		super(props);
		this.state = {
			contractData: [],
			contarctRequest: {
				collaboratorFilter: false,
				supplierNameFilter: [],
				contractOwnerFilter: [],
				statusFilter: [],
				sortBy: 'modificationDate',
				pageRequest: { pageNo: 1, pageSize: 50 },
				sortType: 'DESC',
				type: '',
				validityEndDate: null,
				validityStartDate: null
			},
			query: '',
			sortType: 'DESC',
			searchIcon: true,
			showLoader: false,
			showDialog: false
		};
	}

	fetchContracts() {
		this.setState({ showLoader: true });
		axios
			.post(
				BASE.URL + BASE.URL_LIST.CONTRACT.GET_ALL,
				this.state.contarctRequest
			)
			.then(res => {
				const data = res['data'];
				if (data.status) {
					if (data.response.hits.hits.length > 0) {
						this.setState({
							contractData: data.response.hits.hits,
							showLoader: false
						});
					}
				} else {
					alert('failed to fetch data');
				}
			});
	}

	searchContracts(searchString: string) {
		let obj = {
			...this.state.contarctRequest,
			query: searchString
		};
		this.setState({ showLoader: true });

		axios.post(BASE.URL + BASE.URL_LIST.CONTRACT.SEARCH, obj).then(res => {
			const data = res['data'];
			if (data.status) {
				this.setState({
					contractData: data.response.hits.hits,
					showLoader: false
				});
			} else {
				alert('failed to search data');
			}
		});
	}

	handleInputChangePress = (value?: any) => {
		this.setState({ query: value });
	};

	handleInputKeyUpEvent = (event?: any, target?: any) => {
		if (event === 'Enter') {
			if (target.value.length > 0) {
				const searchString = target.value;
				this.searchContracts(searchString);
			}
		} else if (target.value.length === 0) {
			this.fetchContracts();
			this.setState({ searchIcon: true });
		} else if (target.value.length > 0) {
			this.setState({ searchIcon: false });
		}
	};

	handleSelectChange(val?: any) {
		const contarctRequest: any = Object.assign({}, this.state.contarctRequest);
		val === '1'
			? contarctRequest.contractOwnerFilter.push('Abhishek Kumar')
			: contarctRequest.contractOwnerFilter.pop();
		this.setState({ contarctRequest });
		this.fetchContracts();
	}

	resetSearch = () => {
		const contarctRequest: any = {
			collaboratorFilter: false,
			supplierNameFilter: [],
			contractOwnerFilter: [],
			statusFilter: [],
			type: '',
			sortBy: 'modificationDate',
			pageRequest: { pageNo: 1, pageSize: 50 },
			sortType: 'DESC',
			validityEndDate: null,
			validityStartDate: null
		};
		this.setState({ contarctRequest, query: '', searchIcon: true });
		this.fetchContracts();
	};

	showConfirmPopup = (flag?: boolean) => {
		this.setState({ showDialog: true });
	};

	handleDialogResponse = (response: boolean) => {
		if (response) {
			this.resetSearch();
		}
		this.setState({ showDialog: false });
	};

	selectSearchType = (val: string) => {
		const contarctRequest: any = Object.assign({}, this.state.contarctRequest);
		contarctRequest.type = val;
		this.setState({ contarctRequest });
	};

	componentDidMount() {
		this.fetchContracts();
	}

	handleSortType = (val: string) => {
		const contarctRequest: any = Object.assign({}, this.state.contarctRequest);
		contarctRequest.sortType = val;
		this.setState({ contarctRequest, sortType: val });
		this.fetchContracts();
	};

	render() {
		return (
			<div className='container-fluid Landing'>
				{this.state.showDialog ? (
					<ConfirmDialog
						dialogText='Are you sure, you want to reset your search?'
						handleDialogResponse={this.handleDialogResponse}
					/>
				) : null}
				<FilterContract
					handleInputKeyUpEvent={this.handleInputKeyUpEvent}
					handleInputChangePress={this.handleInputChangePress}
					handleSelectChange={this.handleSelectChange.bind(this)}
					searchIcon={this.state.searchIcon}
					showConfirmPopup={this.showConfirmPopup}
					query={this.state.query}
					selectSearchType={this.selectSearchType}
				/>
				{this.state.contractData.length > 0 ? (
					<ContractTable
						sortType={this.state.sortType}
						handleSortType={this.handleSortType}
						contractData={this.state.contractData}
					/>
				) : (
					<div className='col-sm-12'>
						<div className='noData'>
							<span>Your Search returned zero contract Bitch...</span>
						</div>
					</div>
				)}
				{this.state.showLoader ? <Loader /> : null}
			</div>
		);
	}
}
