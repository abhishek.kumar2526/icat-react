import React from 'react';
import './ContractTable.scss';

const slicePipe = (string: string, num: number) => {
	return string && string.length > num ? string.slice(0, num) : string;
};

const dateFormatter = (date: any) => {
	const month = [
		'Jan',
		'Feb',
		'Mar',
		'April',
		'May',
		'June',
		'July',
		'August',
		'Sept',
		'Oct',
		'Nov',
		'Dec'
	];
	var newdate = new Date(parseFloat(date));
	let d =
		newdate.getDate() +
		1 +
		'-' +
		month[newdate.getMonth()] +
		'-' +
		newdate.getFullYear();
	return d;
};

export const ContractTable = (props: any) => {
	return (
		<div className='row ContractTable'>
			<div className='col-sm-12'>
				<table>
					<tbody>
						<tr>
							<th>
								Master Agreement Name/ID
								{props.sortType === 'DESC' ? (
									<i
										className='fas fa-arrow-down'
										onClick={props.handleSortType.bind(props, 'ASC')}
									/>
								) : (
									<i
										className='fas fa-arrow-up'
										onClick={props.handleSortType.bind(props, 'DESC')}
									/>
								)}
							</th>
							<th>
								Supplier Name/ID
								{/* <i className='fas fa-chevron-circle-down' /> */}
							</th>
							<th>
								Validity Date
								{/* <i className='fas fa-chevron-circle-down' /> */}
							</th>
							<th>
								Contract Owner
								{/* <i className='fas fa-chevron-circle-down' /> */}
							</th>
							<th>
								Contract Type
								{/* <i className='fas fa-chevron-circle-down' /> */}
							</th>
							<th>Buying Category</th>
							<th>Buyer Role</th>
							<th>
								Contract Status
								{/* <i className='fas fa-chevron-circle-down' /> */}
							</th>
							<th>Last Modified</th>
						</tr>
						{props.contractData.map((res: any) => (
							<tr key={res.id}>
								<td>
									<p
										className='text-blue contract-name'
										title={res.sourceAsMap.contractName}
									>
										{slicePipe(res.sourceAsMap.contractName, 15)}...
									</p>
									<small className='text-grey owner'>
										{res.sourceAsMap.iCatReferenceNumber}
									</small>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<small className='text-grey owner'>
										{res.sourceAsMap.contractVersion || 'v1'}
									</small>
								</td>
								<td>
									<p className='supplier-name'>
										{slicePipe(res.sourceAsMap.supplierName, 15)}
									</p>
									<small>{res.sourceAsMap.supplierCode}</small>
								</td>
								<td>
									<p className='contract-date'>
										{dateFormatter(res.sourceAsMap.effectiveDate)}
									</p>
									<small className='text-grey contract-date'>
										{dateFormatter(res.sourceAsMap.expirationDate)}
									</small>
								</td>
								<td>{res.sourceAsMap.contractOwner}</td>

								<td>{res.sourceAsMap.ctcType === '2' ? 'Date' : 'Volume'}</td>
								<td>
									{res.sourceAsMap.network ? res.sourceAsMap.network : ''}&nbsp;
									{res.sourceAsMap.portfolio ? res.sourceAsMap.portfolio : ''}
								</td>
								<td>Contract Owner</td>
								<td>
									{res.sourceAsMap.contractStatusHistoryList.length < 5 ? (
										<i className='fas fa-hourglass-start' />
									) : (
										<i className='fas fa-hourglass-start no-opacity' />
									)}
									{res.sourceAsMap.contractStatusHistoryList.length > 0 &&
										res.sourceAsMap.contractStatusHistoryList[0].contractStatus}
								</td>
								<td>{res.sourceAsMap.contractOwner}</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		</div>
	);
};
