import React from 'react';
import './FilterContract.scss';

export const FilterContract = (props: any) => {
	return (
		<div className='row Filter'>
			<div className='col-sm-8 left-part'>
				<span className='font-500'>All Contracts</span>
				<span className='searchContainer'>
					<select onChange={e => props.selectSearchType(e.target.value)}>
						<option value=''>Contract</option>
						<option value='material'>Material</option>
						<option value='plant'>Plant</option>
						<option value='supplier'>Supplier</option>
					</select>
					<input
						id='searchInput'
						type='text'
						placeholder='Search Contarcts'
						onChange={e => props.handleInputChangePress(e.target.value)}
						onKeyUp={e => props.handleInputKeyUpEvent(e.key, e.target)}
						value={props.query}
					/>
					{props.searchIcon ? (
						<i className='fas fa-search' />
					) : (
						<i
							className='fas fa-times'
							onClick={props.showConfirmPopup.bind(props, true)}
						/>
					)}
					<span className='search-caption'>
						Search with Material, Supplier and Plant etc.
					</span>
				</span>
			</div>
			<div className='col-sm-4 right-part'>
				<button>CREATE CONTRACT</button>
				<select onChange={e => props.handleSelectChange(e.target.value)}>
					<option value={0}>All Contracts</option>
					<option value={1}>My Contracts</option>
				</select>
			</div>
		</div>
	);
};
