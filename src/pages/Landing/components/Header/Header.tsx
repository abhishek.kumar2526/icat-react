import React from 'react';
import './Header.scss';
import { NavLink } from 'react-router-dom';

export const Header = (props: any) => {
	return (
		<div className='row Header'>
			<div className='col-sm-12'>
				<img
					alt='logo'
					src='http://smartccfuidev.moglilabs.com/assets/img/Logo_unilever.svg'
				/>
				<ul className='links'>
					<li>
						<NavLink to='/dashboard' activeClassName='active'>
							<i className='fab fa-slack' />
							Dashboard
						</NavLink>
					</li>
					<li>
						<NavLink to='/landing-page' activeClassName='active'>
							<i className='fas fa-box' />
							Contracts
						</NavLink>
					</li>
					<li>
						<NavLink to='/settings' activeClassName='active'>
							<i className='fas fa-cog' />
							Settings
						</NavLink>
					</li>

					<li>
						<NavLink to='/cpmo' activeClassName='active'>
							<i className='fas fa-user-friends' />
							Cpmo
						</NavLink>
					</li>
				</ul>
			</div>
		</div>
	);
};
