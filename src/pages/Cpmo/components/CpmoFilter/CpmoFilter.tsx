import React from 'react';
import './CpmoFilter.scss';

export const CpmoFilter = (props: any) => {
	return (
		<div className='row FilterCpmo'>
			<div className='col-sm-8 left-part'>
				<span className='searchContainer'>
					<select onChange={e => props.selectSearchType(e.target.value)}>
						<option value='clm'>CLM</option>
						<option value='icatRef'>ICAT</option>
						<option value='cpmo'>CPMO</option>
					</select>
					<input
						id='searchInput'
						type='text'
						placeholder='Search CTC Numbers/Incident number'
						onChange={e => props.handleInputChangePress(e.target.value)}
						onKeyUp={e => props.handleInputKeyUpEvent(e.key, e.target)}
						value={props.searchString}
					/>
					{props.searchIcon ? (
						<i className='fas fa-search' />
					) : (
						<i
							className='fas fa-times'
							onClick={props.showConfirmPopup.bind(props, true)}
						/>
					)}
				</span>
			</div>
			<div className='col-sm-4 right-part'>
				<button>Unlock</button>
			</div>
		</div>
	);
};
