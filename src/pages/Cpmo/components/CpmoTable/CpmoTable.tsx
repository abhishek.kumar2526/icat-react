import React from 'react';
import './CpmoTable.scss';

export const CpmoTable = (props: any) => {
	return (
		<div className='row CpmoTable'>
			<div className='col-sm-12'>
				<div className='content'>
					{Object.keys(props.cpmoData).length === 0 ? (
						<p className='placeholder'>Search CTC Number to Proceed</p>
					) : (
						<div>
							<div className='cpmoHeader'>
								<div className='col-sm-2 cpmo-header-content'>
									<p>Contract ID:</p>
									<p>{props.cpmoData.maId || 'NA'}</p>
								</div>
								<div className='col-sm-2 cpmo-header-content'>
									<p>Contract Name:</p>
									<p>{props.cpmoData.contractName}</p>
								</div>
								<div className='col-sm-2 cpmo-header-content'>
									<p>Contract Status:</p>
									<p>{props.cpmoData.contractStatus}</p>
								</div>
								<div className='col-sm-2 cpmo-header-content'>
									<p>Incident Number:</p>
									<p>{props.cpmoData.iCatReferenceNumber}</p>
								</div>
								<div className='col-sm-2 cpmo-header-content'>
									<p>Email ID</p>
									<p>{props.cpmoData.email}</p>
								</div>
								<div className='col-sm-2 cpmo-header-content'>
									<p>Request Type</p>
									<p>New</p>
								</div>
							</div>
							<div className='cpmoData'>
								<ul>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>CLM input file Header</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>CLM input file line items</span>
										</div>
										{/* <div className='right_part'>
										Download
										<i className='fas fa-arrow-down' />
									</div> */}
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (true ? 'text-blue' : null)
												}
											/>
											<span>Executive Summary</span>
										</div>
										<div className='right_part'>
											Download
											<i className='fas fa-arrow-down' />
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (true ? 'text-blue' : null)
												}
											/>
											<span>PAS Document</span>
										</div>
										<div className='right_part'>
											Download
											<i className='fas fa-arrow-down' />
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Upstream Sourcing</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Quota Allocation</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Bespoke</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>OTM (for Cordillera)</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Legal Consultation</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (true ? 'text-blue' : null)
												}
											/>
											<span>Price Conditions directly in ECC</span>
										</div>
										<div className='right_part'>
											NO
											{/* <i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (true ? 'text-blue' : null)
												}
											/>
											<span>Supplier Signature Required</span>
										</div>
										<div className='right_part'>
											YES
											{/* <i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Supplier USQS Compliant</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Additional Information</span>
										</div>
										<div className='right_part'>
											{/* Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (true ? 'text-blue' : null)
												}
											/>
											<span>Emergency Contracts</span>
										</div>
										<div className='right_part'>
											: YES
											{/* <i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (true ? 'text-blue' : null)
												}
											/>
											<span>Line Items Details</span>
										</div>
										<div className='right_part'>
											: NO
											{/* <i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (true ? 'text-blue' : null)
												}
											/>
											<span>Supporting Documents</span>
										</div>
										<div className='right_part'>
											Download
											<i className='fas fa-arrow-down' />
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Line Item Comments</span>
										</div>
										<div className='right_part'>
											{/* : Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
									<li>
										<div className='left_part'>
											<i
												className={
													'fas fa-check-circle ' + (false ? 'text-blue' : null)
												}
											/>
											<span>Different Item Type</span>
										</div>
										<div className='right_part'>
											{/* : Download
										<i className='fas fa-arrow-down' /> */}
										</div>
									</li>
								</ul>
							</div>
						</div>
					)}
				</div>
			</div>
		</div>
	);
};
