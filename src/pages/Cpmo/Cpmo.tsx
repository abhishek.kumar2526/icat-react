import React from 'react';
import './Cpmo.scss';
import { CpmoFilter } from './components/CpmoFilter/CpmoFilter';
import { ConfirmDialog } from '../../shared/components/ConfirmDialog/ConfirmDialog';
import { CpmoTable } from './components/CpmoTable/CpmoTable';
import { Loader } from '../../shared/components/Loader/Loader';
import Axios from 'axios';
import { BASE } from '../../config/baseUrl';

interface MyComponentProps {}
interface MyComponentState {
	showLoader: Boolean;
	showDialog: Boolean;
	searchIcon: Boolean;
	searchString: String;
	cpmoRequest: Object;
	cpmoData: Object;
}

export class CpmoPage extends React.Component<
	MyComponentProps,
	MyComponentState
> {
	constructor(props: any) {
		super(props);
		this.state = {
			searchIcon: true,
			searchString: '',
			showLoader: true,
			showDialog: false,
			cpmoRequest: { type: 'clm' },
			cpmoData: {}
		};
	}

	componentDidMount() {
		setTimeout(() => {
			this.setState({ showLoader: false });
		}, 1000);
	}

	selectSearchType = (val: string) => {
		const cpmoRequest: any = Object.assign({}, this.state.cpmoRequest);
		cpmoRequest.type = val;
		this.setState({ cpmoRequest });
	};
	handleInputChangePress = (searchString: string) => {
		this.setState({ searchString });
	};
	showConfirmPopup = (val: Boolean) => {
		if (val) {
			this.setState({ showDialog: true });
		}
	};
	searchCpmo = (searchString: string) => {
		this.setState({ showLoader: true });
		Axios.post(
			BASE.CORE + BASE.URL_LIST.CPMO.SEARCH,
			{
				...this.state.cpmoRequest,
				id: searchString
			},
			{ ...BASE.USER }
		).then(data => {
			if (data.status) {
				this.setState({ showLoader: false, cpmoData: data.data.ccf });
			}
		});
	};

	handleDialogResponse = (response: Boolean) => {
		if (response) {
			this.setState({ searchString: '' });
		}
		this.setState({ showDialog: false });
	};

	handleInputKeyUpEvent = (event?: any, target?: any) => {
		if (event === 'Enter') {
			if (target.value.length > 0) {
				const searchString = target.value;
				this.searchCpmo(searchString);
			}
		} else if (target.value.length === 0) {
			this.setState({ searchIcon: true, cpmoData: {} });
		} else if (target.value.length > 0) {
			this.setState({ searchIcon: false });
		}
	};

	render() {
		return (
			<div className='Cpmo'>
				{this.state.showDialog ? (
					<ConfirmDialog
						dialogText='Are you sure, you want to reset your search?'
						handleDialogResponse={this.handleDialogResponse}
					/>
				) : null}
				<CpmoFilter
					searchIcon={this.state.searchIcon}
					selectSearchType={this.selectSearchType}
					handleInputChangePress={this.handleInputChangePress}
					handleInputKeyUpEvent={this.handleInputKeyUpEvent}
					showConfirmPopup={this.showConfirmPopup}
					searchString={this.state.searchString}
				/>
				<CpmoTable cpmoData={this.state.cpmoData} />
				{this.state.showLoader ? <Loader /> : null}
			</div>
		);
	}
}
