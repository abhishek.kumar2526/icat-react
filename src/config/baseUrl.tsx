export const BASE = {
	URL: 'http://smartccfsearchdev.moglilabs.com/',
	CORE: 'http://smartccfcoredev.moglilabs.com/',
	URL_LIST: {
		CONTRACT: {
			GET_ALL: 'contracts/getall',
			SEARCH: 'contracts/search'
		},
		CPMO: {
			SEARCH: 'ccf/searchCPMO'
		}
	},
	USER: {
		headers: {
			token:
				'eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiI1MCIsImF1ZCI6IkFiaGlzaGVrIEt1bWFyX2FiaGlzaGVrQHVuaWxldmVyLmNvbSIsInN1YiI6IjZlMGZlODgzNGVmNjQ5N2U4MjdhNDQ0OGQ5ZTJmMjE1IiwiZXhwIjoxNjUxNDgzODUyfQ.6D_kCd5sdJepY2iwLaBKOqBb5Hz7jHcSy0V_CyatpOxkQyu5ALvMZ7mywIzChrWnPuTzKyR0Lx5FYz7AAb8VGg'
		}
	}
};
