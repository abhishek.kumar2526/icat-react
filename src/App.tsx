import './App.scss';
import React, { Component } from 'react';
import { Page } from './pages/Pages';

class App extends Component {
	render() {
		return (
			<div className='App'>
				<Page />
			</div>
		);
	}
}

export default App;
