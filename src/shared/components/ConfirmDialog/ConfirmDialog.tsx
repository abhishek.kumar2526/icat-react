import React from 'react';
import './ConfirmDialog.scss';
export const ConfirmDialog = (props: any) => {
	return (
		<div className='ConfirmDialog'>
			<div className='dialogContent'>
				<i
					className='fas fa-times'
					onClick={props.handleDialogResponse.bind(props, false)}
				/>
				<p>{props.dialogText}</p>
				<div className='btn-container'>
					<button
						onClick={props.handleDialogResponse.bind(props, true)}
						className='btn-yes'
					>
						YES
					</button>
					<button onClick={props.handleDialogResponse.bind(props, false)}>
						NO
					</button>
				</div>
			</div>
		</div>
	);
};
